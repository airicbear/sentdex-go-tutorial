package main

import "fmt"

const uSixteenBitMax float64 = 65535
const kmhMultiple float64 = 1.60934

type car struct {
  gasPedal      uint16 // min 0 max 65535
  brakePedal    uint16
  steeringWheel int16 // -32k - +32k
  topSpeedKMH   float64
}

func (c car) kmh() float64 {
  return float64(c.gasPedal) * (c.topSpeedKMH / uSixteenBitMax)
}

func (c car) mph() float64 {
  return float64(c.gasPedal) * (c.topSpeedKMH / uSixteenBitMax / kmhMultiple)
}

func (c *car) newTopSpeed(newspeed float64) {
  c.topSpeedKMH = newspeed
}

func newerTopSpeed(c car, speed float64) car {
  c.topSpeedKMH = speed
  return c
}

func main() {
  aCar := car{
    gasPedal:      65000,
    brakePedal:    0,
    steeringWheel: 12561,
    topSpeedKMH:   225.0}

  fmt.Println(aCar.gasPedal)
  fmt.Println(aCar.kmh())
  fmt.Println(aCar.mph())

  // YOU CAN DO THIS
  aCar.newTopSpeed(500)
  fmt.Println(aCar.kmh())
  fmt.Println(aCar.mph())
  // OR THE LESS EFFICIENT WAY
  aCar = newerTopSpeed(aCar, 600)
  fmt.Println(aCar.kmh())
  fmt.Println(aCar.mph())
}